﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssistAveugle
{

    enum EnumSide { LEFT, CENTER, RIGHT };

    class Side
    {
        public bool lastFrameIsBusy = false;
        public bool lastFrameIsErrorZero = false;
        private int size = 0;
        private SoundAlert sound;
        public static Dictionary<EnumSide, Side> sides = new Dictionary<EnumSide, Side>();
        public EnumSide enumSide;

        public Side(int size,string  sndLocation, EnumSide enSide)
        {
            this.enumSide = enSide;
            this.sound = new SoundAlert(sndLocation);
            this.size = size;
            Side.sides.Add(enSide, this);
        }

        public static void alertSideBusy(){
            List<Side> sides = Side.sidesBusy();
            int nbreSide = sides.Count();
            if (nbreSide == 1)
            {
                Side side = sides.First();
                if (side.enumSide == EnumSide.CENTER)
                {
                    Side.sides[EnumSide.RIGHT].sound.alert();
                }
                else if (side.enumSide == EnumSide.RIGHT)
                {
                    Side.sides[EnumSide.LEFT].sound.alert();
                }
                else
                {
                    Side.sides[EnumSide.RIGHT].sound.alert();
                }
            }
            else if (nbreSide == 2)
            {
                Console.WriteLine("2");
                sides = Side.sidesFree();
                Console.WriteLine(sides.First().enumSide);
                sides.First().sound.alert();
            }
            else if (nbreSide == 3)
            {
                Side.sides[EnumSide.CENTER].sound.alert();
            }


        }

        public static List<Side> sidesFree()
        {
            List<Side> sides = new List<Side>();
            foreach (KeyValuePair<EnumSide, Side> listSides in Side.sides)
            {
                if (!listSides.Value.lastFrameIsBusy)
                {
                    sides.Add(listSides.Value);
                }
            }
            return sides;
        }

        public static List<Side> sidesBusy(){
            List<Side> sides = new List<Side>();
            foreach (KeyValuePair<EnumSide, Side> listSides in Side.sides)
            {
                if (listSides.Value.lastFrameIsBusy)
                {
                    sides.Add(listSides.Value);
                }
            }
            return sides;
        }

        public int getSize()
        {
            return this.size;
        }

        public bool hasObstacle(int nbDepthBusy)
        {
            if(nbDepthBusy > (0.2 * this.size)){
                this.lastFrameIsBusy = true;
                return true;
            }
            /*else{
                if (this.lastFrameIsErrorZero)
                {
                    return true;
                }
                this.lastFrameIsBusy = false;
                return false;
            }*/
            return false;
        }

        public void setErrorZero(int nbDepthZero)
        {
            if (nbDepthZero > (0.8 * this.size))
            {
                this.lastFrameIsErrorZero = true;
                return;
            }
            this.lastFrameIsErrorZero = false;
        }

    }
}