﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Controls;

namespace AssistAveugle
{
    public class SoundAlert
    {
        private System.Media.SoundPlayer sound;
        private int enough = 0;

        public SoundAlert(string sndLocation)
        {
            this.sound = new System.Media.SoundPlayer();
            this.sound.SoundLocation = sndLocation;
        }

        public void alert()
        {
            if (this.enough > 10)
            {
                
                this.sound.Play();
                this.enough = 0;
            }else{
                this.enough++;
            }
        }

        
    }
}
