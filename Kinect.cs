﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Research.Kinect.Nui;
using System.Runtime.InteropServices;
namespace AssistAveugle
{
    class Kinect
    {
        private MainWindow mainWindow;
        public bool divideDepth = false;
        private int balanceRight = 0;
        private int sampleMax = 0;
        private int sample = 10;

        private Side left;
        private Side center;
        private Side right;

        public int depthMax = 1000;

        public Kinect(MainWindow mainWindow)
        {
            // TODO: Complete member initialization
            this.mainWindow = mainWindow;
            Runtime nui = new Runtime();

            try
            {
                nui.Initialize(RuntimeOptions.UseColor | RuntimeOptions.UseDepth);
            }
            catch (InvalidOperationException)
            {
                System.Windows.MessageBox.Show("Erreur initialisation runtime, kinect non branché");
                return;
            }

            try
            {
                nui.DepthStream.Open(ImageStreamType.Depth, 2, ImageResolution.Resolution320x240, ImageType.Depth);

            }
            catch
            {
                System.Windows.MessageBox.Show("Erreur ouverture de la profondeur");
            }
            nui.DepthFrameReady += new EventHandler<ImageFrameReadyEventArgs>(nui_DepthFrame);
            this.left = new Side(25600, "D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/sounds/left.wav", EnumSide.LEFT);
            this.center = new Side(25600, "D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/sounds/stop.wav", EnumSide.CENTER);
            this.right = new Side(25600, "D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/sounds/right.wav",  EnumSide.RIGHT);
        }


        void nui_DepthFrame(object sender, ImageFrameReadyEventArgs e) // event appellé pour afficher la caméra en profondeur
        {
            byte[] imageAffichage = new byte[e.ImageFrame.Image.Width * e.ImageFrame.Image.Height * 4];
            
            byte[] imageRecue = e.ImageFrame.Image.Bits;
            int leftDepth = 0;
            int rightDepth = 0;
            int centerDepth = 0;
            int leftZero = 0;
            int rightZero = 0;
            int centerZero = 0;
            this.sampleMax++;
            
            for (int iRecu = 0, iAffiche = 0; (iRecu < imageRecue.Length) && (iAffiche < imageAffichage.Length); iRecu += 2, iAffiche += 4)
            {
                int profondeur = (imageRecue[iRecu] | imageRecue[iRecu + 1] << 8);
                    //(imageRecue[iRecu + 1] & 0xF) << 8 | imageRecue[iRecu];
                int countRight = 0;
                const float ProfondeurMin = 200;
                const float ProfondeurMax = 3000;
                const float PlageTraitee = ProfondeurMax - ProfondeurMin;
                byte couleur = (byte)(255 - (255 * Math.Max(Math.Min(profondeur, ProfondeurMax) - ProfondeurMin, 0) / PlageTraitee));
                int caseY = ((int)iRecu/640);
                int pos = (caseY * 640);

                //this.mainWindow.length.Content = e.ImageFrame.Image.Width;

                if (!this.divideDepth)
                {
                    if (profondeur < this.depthMax && profondeur > 0)
                    {
                        imageAffichage[iAffiche] = 0;
                        imageAffichage[iAffiche + 1] = 255;
                        imageAffichage[iAffiche + 2] = 0;
                    }
                    else
                    {
                        imageAffichage[iAffiche] = couleur;
                        imageAffichage[iAffiche + 1] = couleur;
                        imageAffichage[iAffiche + 2] = couleur;
                    }
                }
                
                if (iRecu >= pos && iRecu < pos + 213) // sur la droite de l'aveugle
                {
                    countRight++;
                    if (this.divideDepth)
                    {
                        imageAffichage[iAffiche] = 0;
                        imageAffichage[iAffiche + 1] = 0;
                        imageAffichage[iAffiche + 2] = 255;
                    }
                    if (profondeur < this.depthMax && profondeur > 0)
                    {
                        rightDepth++;
                    }
                    else if (profondeur == 0)
                    {
                        rightZero++;
                    }
                }
                else if (iRecu >= pos + 210 && iRecu < pos + 213 * 2) // au milieu
                {
                    if (this.divideDepth)
                    {
                        imageAffichage[iAffiche] = 0;
                        imageAffichage[iAffiche + 1] = 255;
                        imageAffichage[iAffiche + 2] = 0;
                    }
                    if (profondeur < this.depthMax && profondeur > 0)
                    {
                        //this.mainWindow.length.Content = profondeur;
                        centerDepth++;
                    }
                    else if (profondeur == 0)
                    {
                        centerZero++;
                    }
                }
                else if (iRecu >= pos + 210 * 2 && iRecu < pos + 213 * 3) // à sa gauche
                {
                    if (this.divideDepth)
                    {
                        imageAffichage[iAffiche] = 255;
                        imageAffichage[iAffiche + 1] = 0;
                        imageAffichage[iAffiche + 2] = 0;
                    }
                    if (profondeur < this.depthMax && profondeur > 0)
                    {
                        leftDepth++;
                    }
                    else if (profondeur == 0)
                    {
                        leftZero++;
                    }

                }


            }
            if (this.left.hasObstacle(leftDepth))
            {
                this.mainWindow.vibrorLeft.on();
            }
            else
            {
                this.mainWindow.vibrorLeft.off();
            }
            this.left.setErrorZero(leftZero);
            if (this.right.hasObstacle(leftDepth))
            {
                this.mainWindow.vibrorRight.on();
            }
            else
            {
                this.mainWindow.vibrorRight.off();
            }
            this.right.setErrorZero(rightZero);
            if (this.center.hasObstacle(centerDepth))
            {
                this.mainWindow.vibrorCenter.on();
            }
            else
            {
                this.mainWindow.vibrorCenter.off();
            }
            this.center.setErrorZero(centerZero);
            Side.alertSideBusy();
            this.mainWindow.imgDepth.Source = BitmapSource.Create(e.ImageFrame.Image.Width, e.ImageFrame.Image.Height, 96, 96,
                                PixelFormats.Bgr32, null, imageAffichage, e.ImageFrame.Image.Width * 4);
        }
    }
}