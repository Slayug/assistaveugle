﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Research.Kinect.Nui;
using System.Runtime.InteropServices;
using System.Windows.Controls;

namespace AssistAveugle
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Kinect kinect;
        public Vibror vibrorLeft;
        public Vibror vibrorRight;
        public Vibror vibrorCenter;
        
        public MainWindow()
        {
            InitializeComponent();
            this.imgStickMan.Source = new BitmapImage(new Uri("D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/stickman.jpg", UriKind.Absolute));
            //this.imgVibrorCenter.Source = this.imgVibrorLeft.Source = this.imgVibrorRight.Source = new BitmapImage(new Uri("D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/red.jpg", UriKind.Absolute));
            
        }
        

        private void windowLoaded(object sender, RoutedEventArgs e)
        {
            /*this.alertLeft = new SoundAlert("D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/sounds/right.wav");
            this.alertRight = new SoundAlert("D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/sounds/left.wav");
            this.alertCenter = new SoundAlert("D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/sounds/stop.wav");
            //alertCenter.Play();*/
            this.vibrorLeft = new Vibror(this.imgVibrorLeft);
            this.vibrorRight = new Vibror(this.imgVibrorRight);
            this.vibrorCenter = new Vibror(this.imgVibrorCenter);
            this.kinect = new Kinect(this);
        }

        private void divideClicked(object sender, RoutedEventArgs e)
        {
           if(this.kinect.divideDepth){
                this.kinect.divideDepth = false;
            }else{
                this.kinect.divideDepth = true;
            }
        }
    }
}
