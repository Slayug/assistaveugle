﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Research.Kinect.Nui;
using System.Runtime.InteropServices;
using System.Windows.Controls;

namespace AssistAveugle
{
    public class Vibror
    {

        private bool isOn = false;
        private Image img;
        public static BitmapImage red = new BitmapImage(new Uri("D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/red.jpg", UriKind.Absolute));
        public static BitmapImage green = new BitmapImage(new Uri("D:/Documents/Visual Studio 2010/Projects/AssistAveugle/AssistAveugle/green.jpg", UriKind.Absolute));

        public Vibror(Image _img)
        {
            _img.Source = Vibror.green;
            this.img = _img;
        }


        public void on()
        {
            if (!this.isOn)
            {
                this.isOn = true;
                this.img.Source = Vibror.red;
            }
        }

        public void off()
        {
            if (this.isOn)
            {
                this.isOn = false;
                this.img.Source = Vibror.green;
            }
        }
    }
}
